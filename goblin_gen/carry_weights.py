class CarryWeightCalc:

    @staticmethod
    def get_max_carry_weight(str):
        maxCarryWeights = {
            1: 10,
            2: 20,
            3: 30,
            4: 40,
            5: 50,
            6: 60,
            7: 70,
            8: 80,
            9: 90,
            10: 100,
            11: 115,
            12: 130,
            13: 150,
            14: 175,
            15: 200,
            16: 230,
            17: 260,
            18: 300,
            19: 350,
            20: 400
        }
        return maxCarryWeights.get(str, 1000)

    @staticmethod
    def get_med_carry_weight(str):
        return (CarryWeightCalc.get_max_carry_weight(str) * 2) // 3

    @staticmethod
    def get_light_carry_weight(str):
        return CarryWeightCalc.get_max_carry_weight(str) // 3


class SmallCharCarryWeightCalc(CarryWeightCalc):
    @staticmethod
    def get_max_carry_weight(str):
        return (super(SmallCharCarryWeightCalc, SmallCharCarryWeightCalc).get_max_carry_weight(str) * 3) // 4

    @staticmethod
    def get_med_carry_weight(str):
        return (SmallCharCarryWeightCalc.get_max_carry_weight(str) * 2) // 3

    @staticmethod
    def get_light_carry_weight(str):
        return SmallCharCarryWeightCalc.get_max_carry_weight(str) // 3
