"""Constants and generic data classes"""

import random
from typing import List


class _DataStore:
    def as_list(self) -> List[str]:
        return list(self.__dict__.values())

    def get_random(self) -> str:
        return random.choice(self.as_list())


class _Languages(_DataStore):
    """List of all languages a goblin can start knowing"""

    def __init__(self):
        self.GOBLIN = "Goblin"
        self.COMMON = "Common"
        self.DRACONIC = "Draconic"
        self.DWARVEN = "Dwarven"
        self.GNOLL = "Gnoll"
        self.GNOME = "Gnome"
        self.HALFLING = "Halfling"
        self.ORC = "Orc"

    def get_weighted_random(self):
        """Random language with a 50% chance of being common"""
        mod = random.random()
        if mod < 0.5:
            return self.COMMON
        return self.get_random()


class _Hair(_DataStore):
    def __init__(self):
        self.BLONDE = "Blonde"
        self.BROWN = "Brown"
        self.BLUE = "Blue"
        self.BALD = "Bald"
        self.GREY = "Grey"


class _Eyes(_DataStore):
    def __init__(self):
        self.BLUE = "Blue"
        self.RED = "Red"
        self.BROWN = "Brown"


class _Alignments(_DataStore):
    def __init__(self):
        self.LG = "Lawful Good"
        self.NG = "Neutral Good"
        self.CG = "Chaotic Good"
        self.LN = "Lawful Neutral"
        self.N = "Neutral"
        self.CN = "Chaotic Neutral"
        self.LE = "Lawful Evil"
        self.NE = "Neutral Evil"
        self.CE = "Chaotic Evil"

        self.expected = self.NE
        self.common = [self.N, self.CE, self.LE]
        self.unlikely = [self.LN, self.LG, self.NG, self.CG, self.CN]

    def get_weighted_random(self):
        """
        Generate an alignment
        70% chance of NE
        25% chance of one step away
        5% chance of a random alignment

        in a party of 4 this gives a:
            32% chance of all NE
            81% chance of NE or within one step
        """
        mod = random.random()
        if mod < 0.7:
            return self.NE
        elif mod < 0.95:
            return random.choice(self.common)
        else:
            return random.choice(self.unlikely)


languages = _Languages()
hair = _Hair()
eyes = _Eyes()
alignments = _Alignments()
