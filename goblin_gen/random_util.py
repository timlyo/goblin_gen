"""Wrapper around the standard random, implements some nicer interfaces"""

import random


def get_int(average: int, deviation: int = 3) -> int:
    number = round(random.normalvariate(average, deviation))
    if number < 1:
        return 1
    else:
        return number


